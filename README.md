# Maze Problem Solving Application

[![pipeline status](https://gitlab.com/tdkha/maze/badges/main/pipeline.svg)](https://gitlab.com/tdkha/maze/-/commits/main)

This is a web application built from the inspiration of 42 Code School problem solving sets. The project is a front-end based web application implementing the **Call Stack** data structure to mimic the functionality of a programming language

## Technology Stack

[![My Skills](https://skillicons.dev/icons?i=html,css,js,gitlab,docker)](https://skillicons.dev)

## Getting Started

To run the application locally, you will need to have Node.js and npm installed. Clone the repository and run the following commands in the project directory:

Initializing Front end:

```bash
cd maze

npm install 

npm run start
```

This will start the front end and open the app in your default browser at http://localhost:5173 using **Vite**.

Running End-To-End Test Using Cypress:

```bash

npm run e2e

```

This will utilize the running front-end service to test the behaviors of our application.

Containerizing The Application:

```bash

docker compose up -d

```
