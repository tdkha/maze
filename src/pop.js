window.addEventListener("load", function () {
    const countdownElement = document.getElementById("demo");
    //--------------------------------------------------------------
    // Function to update the timer display
    //--------------------------------------------------------------
    function updateTimerDisplay(remainingTime) {
        const minutes = Math.floor(remainingTime / 60000);
        const seconds = Math.floor((remainingTime % 60000) / 1000);
        countdownElement.innerText = `${minutes}:${seconds < 10 ? "0" : ""}${seconds}`;
    }
    //--------------------------------------------------------------
    // Function to start the 15-minute timer
    //--------------------------------------------------------------
    function startTimer(initialRemainingTime) {
        let remainingTime = initialRemainingTime; // Use the provided remaining time

        // Update the display immediately
        updateTimerDisplay(remainingTime);

        const timer = setInterval(function () {
            remainingTime -= 1000; // Decrease remaining time by 1 second
            //--------------------------------------------------------------
            // Update the remaining time in local storage for the current level
            //--------------------------------------------------------------
            localStorage.setItem(`remainingTimeLevel${getCurrentLevel()}`, remainingTime);

            if (remainingTime <= 0) {
                //--------------------------------------------------------------
                // Timer ended, show the popup only if the end message is not showing
                //--------------------------------------------------------------
                const endMessageShowing = document.querySelector(".end-msg") !== null;
                if (!endMessageShowing) {
                    document.querySelector(".popup").style.display = "block";
                    countdownElement.innerText = "Time is up!";
                    //--------------------------------------------------------------
                    // Remove the remaining time from local storage for the current level
                    //--------------------------------------------------------------
                    localStorage.removeItem(`remainingTimeLevel${getCurrentLevel()}`);
                    //--------------------------------------------------------------
                    // Return the player to level 0
                    //--------------------------------------------------------------
                    localStorage.setItem("level", 0);
                }

                clearInterval(timer); // Clear the interval
            } else {
                updateTimerDisplay(remainingTime);
            }
        }, 1000);
    }
    //--------------------------------------------------------------
    // Function to get the current level
    //--------------------------------------------------------------
    function getCurrentLevel() {
        return parseInt(localStorage.getItem("level") || 0, 10);
    }
    //--------------------------------------------------------------
    // Function to get the remaining time for the current level
    //--------------------------------------------------------------
    function getRemainingTimeForCurrentLevel() {
        return parseInt(localStorage.getItem(`remainingTimeLevel${getCurrentLevel()}`) || 15 * 60 * 1000, 10);
    }
    //--------------------------------------------------------------
    // Check if it"s the user"s first visit
    //--------------------------------------------------------------
    const isFirstVisit = localStorage.getItem("firstVisit") === null;

    if (isFirstVisit) {
        // Show the popup for the first visit
        document.querySelector(".popup").style.display = "block";
        // Set the flag indicating that it"s not the first visit anymore
        localStorage.setItem("firstVisit", "false");
    } else {
        // Continue the timer from where it stopped in the current level
        startTimer(getRemainingTimeForCurrentLevel());
    }
    //--------------------------------------------------------------
    // Handle the "Start" button click
    //--------------------------------------------------------------
    document.getElementById("start-btn").addEventListener("click", function () {
        // Close the popup
        document.querySelector(".popup").style.display = "none";

        // Continue the timer from where it stopped in the current level
        startTimer(getRemainingTimeForCurrentLevel());
    });
    //--------------------------------------------------------------
    // Handle the popup close button click
    //--------------------------------------------------------------
    document.querySelector("#close").addEventListener("click", function () {
        document.querySelector(".popup").style.display = "none";
    });
});
